package Model;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

public class Pedidos {

		
		private int idPedido;
	    private String preco;
	    private String vencimento;
	    
	    public int getIdPedido() {
			return idPedido;
		}
		public void setIdPedido(int idPedido) {
			this.idPedido = idPedido;
		}
	        public String getPreco() {
			return preco;
		}
		public void setPreco(String preco) {
			this.preco = preco;
		}
	        public String getVencimento() {
			return vencimento;
		}
		public void setVencimento(String vencimento) {
			this.vencimento = vencimento;
		}
	}


